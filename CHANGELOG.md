# Changelog

## [v0.1.3]

- fixed empty sshkey handler for storage with private template creation in
server add
- Allowed creating host to choose isoimage which available in gridscale user account

## [v0.1.2]

- Commented out publishing protection in gemfile.

## [v0.1.1]

- Fix the interface collection and model.

## [v0.1.0] 

- Initial release of fog-gridscale.

